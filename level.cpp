#include <SDL2/SDL.h>
#include "level.h"
#include <fstream>
#include <chrono>
#include <queue>
#include <cmath>
#include <ios>
#include "constants.h"
#include "object.h"
#include "bricks.h"
#include "point.h"
#include "utils.h"
#include "borders.h"
#include "slot.h"
#include "stairs.h"
#include "coin.h"
#include <iostream>//


Level::Level() : 
  immovables(Constant::rows, Constant::columns), hero(new Character(Utils::hero_textures)), exit(new Door())
{
  last_move = std::chrono::steady_clock::now();
  last_frame = std::chrono::steady_clock::now();
}

void Level::load_level(const std::string& path)
{
  std::ifstream file(path);

  if (file.fail())
    throw std::ios_base::failure("Level file doesn't exist\n");

  coins = 0;
  collected_coins = 0;
  unsigned object_num = 0;

  for (unsigned row = 0; row < Constant::rows; ++row)
    for (unsigned col = 0; col < Constant::columns; ++col)
    {
      file >> object_num;

      if (object_num == int(Constant::Slot_type::Bricks))
      {
        immovables.set(row, col, std::shared_ptr<Bricks> (
          new Bricks(col * Constant::slot_size, row * Constant::slot_size)));

        objects.push_back(immovables.get(row, col));
      }

      else if (object_num == int(Constant::Slot_type::Hero))
        hero->set_position(col * Constant::slot_size, row * Constant::slot_size);

      else if (object_num == int(Constant::Slot_type::Stairs))
      {
        immovables.set(row, col, std::shared_ptr<Stairs> (new Stairs(col * Constant::slot_size, row * Constant::slot_size)));

        objects.push_back(immovables.get(row, col));
      }

      else if (object_num == int(Constant::Slot_type::Coin)) 
      {
        money.push_back(std::shared_ptr<Coin> (
          new Coin(
            col * Constant::slot_size + (Constant::slot_size - Constant::coin_size) / 2,
            row * Constant::slot_size + Constant::slot_size - Constant::coin_size * Constant::coin_proportion)));

        objects.push_back(money[money.size() - 1]);

        ++coins;
      }

      else if (object_num == int(Constant::Slot_type::Door))
        exit->set_position(col * Constant::slot_size, row * Constant::slot_size);
    }

  objects.push_back(exit);
  objects.push_back(hero);
}

void Level::run(const std::string& level_path, SDL_Renderer* ren)
{
  load_level(level_path);

  while (true) 
  {
    SDL_Event e;

    if (SDL_PollEvent(&e)) 
      if (e.type == SDL_QUIT or (e.type == SDL_KEYDOWN and e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)) 
        break;

    move_objects(e);
    if (has_won())
      break;//change to YOU WON screen  
 
    display_all(ren);
  }
}

void Level::move_objects(SDL_Event& e)
{
  Utils::chrono_time_point now = std::chrono::steady_clock::now();
  int milliseconds_since = std::chrono::duration_cast<std::chrono::milliseconds>(now - last_move).count();

  if (milliseconds_since >= Constant::milliseconds_per_move)
  {  
    move_hero(e);
    last_move = std::chrono::steady_clock::now();
  }
}

void Level::collect_coins()
{
  for (unsigned i = 0; i < money.size(); ++i)
    if (Utils::are_colliding(hero->get_borders(), money[i]->get_borders()))
    {
      collected_coins += money[i]->get_value();
      money[i]->set_visible(false);
      std::swap(money[i], money[money.size() - 1]);
      money.pop_back();
    }
}

bool Level::is_all_money_collected()
{
  if (collected_coins == coins)
    exit->set_open(true);

  return (coins == collected_coins);
}

bool Level::has_won()
{
  if (is_all_money_collected() and Utils::are_colliding(hero->get_borders(), exit->get_borders()))
    return true;

  return false;
}

void Level::move_hero(SDL_Event& e)//fix that hero can go left and up into bricks from stairs
{
  collect_coins();

  if (is_falling(hero.get()))
  {
    hero->move(0, 1);
    hero->set_facing_state(Utils::Facing::Front);
  }
  else
  { 
    Point direction = get_input(e);

    if ((direction.x or direction.y) and can_move(hero.get(), direction)) 
      hero->move(direction.x, direction.y);
    else
      hero->move(0, 0);
  }
}

Point Level::calculate_steps(Point pos, Point to_go)
{
  Point steps;

  if (to_go.y > pos.y)
    steps.y = 1;
  else if (to_go.y < pos.y)
    steps.y = -1;

  if (to_go.x > pos.x)
    steps.x = 1;
  else if (to_go.x < pos.x)
    steps.x = -1;

  return steps;
}

bool Level::is_falling(const Object* obj)
{
  using Utils::get_slot_pos;
  Borders b = obj->get_borders();

  const Object 
    * const bottom1 = immovables.get(get_slot_pos(b.end.y), get_slot_pos(b.start.x)).get(),
    * const bottom2 = immovables.get(b.end).get(),
    * const under1 = immovables.get(get_slot_pos(b.start, 0, 1)).get(),
    * const under2 = immovables.get(get_slot_pos(b.start.y, 1), get_slot_pos(b.end.x)).get();

  bool 
    falling1 = true,
    falling2 = true;
 
  if (under1 != nullptr)
    if (Utils::are_touching(under1->get_borders(), b))
      falling1 = false;

  if (under2 != nullptr)
    if (Utils::are_touching(under2->get_borders(), b)) 
      falling2 = false;

  if (falling1 and falling2)
  {
    if (falling1 and bottom1 != nullptr and Utils::are_colliding(bottom1->get_borders(), b) and bottom1->can_walk_through())
      falling1 = false;

    if (falling2 and bottom2 != nullptr and Utils::are_colliding(bottom2->get_borders(), b) and bottom2->can_walk_through()) 
      falling2 = false;
  }
 
  return (falling1 and falling2);
}

bool Level::can_move(const Object* obj, const Point& direction)
{ 
  Borders b = obj->get_borders();

  if (direction.x > 0)
  {
    return can_move_right(obj);
  }
  else if (direction.x < 0)
  {
    return can_move_left(obj);
  }
  else if (direction.y > 0)
    return can_move_down(obj);

  else 
    return can_move_up(obj);

  return false;
}

bool Level::can_move_right(const Object* o)
{
  using Utils::get_slot_pos;

  Borders b = o->get_borders();

  const Object 
    * const rtop = immovables.get(get_slot_pos(b.start.y), get_slot_pos(b.end.x, 1)).get(),
    * const rbottom = immovables.get(get_slot_pos(b.end, 1, 0)).get();

  bool 
    can1 = (rtop == nullptr or !Utils::are_touching(b, rtop->get_borders()) or rtop->can_walk_through()),
    can2 = (rbottom == nullptr or !Utils::are_touching(b, rbottom->get_borders()) or rbottom->can_walk_through());

  return (can1 and can2);
} 

bool Level::can_move_left(const Object* o)
{
  using Utils::get_slot_pos;

  Borders b = o->get_borders();

  const Object
    * const ltop = immovables.get(get_slot_pos(b.start, -1, 0)).get(),
    * const lbottom = immovables.get(get_slot_pos(b.end.y), get_slot_pos(b.start.x, -1)).get();

  bool 
    can1 = (ltop == nullptr or !Utils::are_touching(b, ltop->get_borders()) or ltop->can_walk_through()),
    can2 = (lbottom == nullptr or !Utils::are_touching(b, lbottom->get_borders()) or lbottom->can_walk_through());

  return (can1 and can2);
}

bool Level::can_move_down(const Object* o)
{
  using Utils::get_slot_pos;

  Borders b = o->get_borders();

  const Object  
    * const lbottom = immovables.get(get_slot_pos(b.end.y), get_slot_pos(b.start.x)).get(),
    * const rbottom = immovables.get(b.end).get(),
    * const lunder = immovables.get(get_slot_pos(b.start, 0, 1)).get(),
    * const runder = immovables.get(get_slot_pos(b.start.y, 1), get_slot_pos(b.end.x)).get();

  bool 
    blocked1 = (lunder != nullptr and Utils::are_touching(lunder->get_borders(), b) and !lunder->can_walk_through()),
    blocked2 = (runder != nullptr and Utils::are_touching(runder->get_borders(), b) and !runder->can_walk_through());

  if (!(blocked1 or blocked2))
  {
    bool stairs1 = ( 
      lbottom != nullptr and lbottom->can_walk_through() and 
      (Utils::are_colliding(lbottom->get_borders(), b) or Utils::are_touching(lbottom->get_borders(), b)) );
    bool stairs2 = ( 
      rbottom != nullptr and rbottom->can_walk_through() and 
      (Utils::are_colliding(rbottom->get_borders(), b) or Utils::are_touching(rbottom->get_borders(), b)) );
    bool stairs3 = ( 
      lunder != nullptr and lunder->can_walk_through() and 
      (Utils::are_colliding(lunder->get_borders(), b) or Utils::are_touching(lunder->get_borders(), b)) );
    bool stairs4 = ( 
      runder != nullptr and runder->can_walk_through() and 
      (Utils::are_colliding(runder->get_borders(), b) or Utils::are_touching(runder->get_borders(), b)) );
  
    return (stairs1 or stairs2 or stairs3 or stairs4);
  }

  return false;
}

bool Level::can_move_up(const Object* o)
{
  using Utils::get_slot_pos;

  Borders b = o->get_borders();

  Object const 
    * const labove = immovables.get(get_slot_pos(b.start, 0, -1)).get(),
    * const rabove = immovables.get(get_slot_pos(b.start.y, -1), get_slot_pos(b.end.x)).get(),
    * const lbottom = immovables.get(get_slot_pos(b.end.y), get_slot_pos(b.start.x)).get(),
    * const rbottom = immovables.get(get_slot_pos(b.end)).get();

  bool 
    blocked1 = (labove != nullptr and Utils::are_touching(labove->get_borders(), b) and !labove->can_walk_through()),
    blocked2 = (rabove != nullptr and Utils::are_touching(rabove->get_borders(), b) and !rabove->can_walk_through());

  if (!(blocked1 or blocked2))
  {
    bool stairs1 = ( 
      lbottom != nullptr and lbottom->can_walk_through() and 
      (Utils::are_colliding(lbottom->get_borders(), b) or Utils::are_touching(lbottom->get_borders(), b)) );
    bool stairs2 = (
      rbottom != nullptr and rbottom->can_walk_through() and 
      (Utils::are_colliding(rbottom->get_borders(), b) or Utils::are_touching(rbottom->get_borders(), b)) );

    return (stairs1 or stairs2);
  }

  return false;
}

int Level::calc_hypo_dist(const Point start, const Point finish)
{
  int distance; 
  distance = abs(start.x - finish.x)/Constant::slot_size + abs(start.y - finish.y)/Constant::slot_size;

  return distance;
}

template <class Comprator> 
void Level::add_slot(
  const Point pos, std::shared_ptr<Slot> active, const Point& end_pos, std::priority_queue<std::shared_ptr<Slot>,
  std::vector<std::shared_ptr<Slot>>, Comprator>& open_slots, Slot_map<std::shared_ptr<Slot>>& open_map)
{
  using Utils::get_slot_pos;

  std::shared_ptr<Slot> slot(new Slot {pos, active->real_dist + 1, calc_hypo_dist(pos, end_pos), active.get()});
  std::shared_ptr<Slot> old_slot = open_map.get(get_slot_pos(pos));

  if (old_slot.get() != nullptr and old_slot->full_dist > slot->full_dist)
  {
    old_slot->real_dist = slot->real_dist;
    old_slot->hypo_dist = slot->hypo_dist;
    old_slot->full_dist = old_slot->real_dist + old_slot->hypo_dist;
    old_slot->parent = slot->parent;
  }

  else if (old_slot.get() == nullptr)
  {
    open_map.set(get_slot_pos(pos), slot);
    open_slots.push(slot);
  }

  if (slot->pos.x < 0 or slot->pos.x > Constant::slot_size * Constant::columns or slot->pos.y < 0 or slot->pos.y > Constant::slot_size * Constant::rows)
    throw std::ios_base::failure("Weird point");
}

template <class Comprator> 
void Level::append_open_slots(
  std::priority_queue<std::shared_ptr<Slot>, std::vector<std::shared_ptr<Slot>>, Comprator>& open_slots, 
  Slot_map<std::shared_ptr<Slot>>& open_map, std::shared_ptr<Slot>& active, const std::shared_ptr<Slot>& end) 
{
  using Utils::get_slot_pos;

  if (get_slot_pos(active->pos.y) >= 0 and get_slot_pos(active->pos.y) + 1 < Constant::rows and 
      get_slot_pos(active->pos.x) >= 0 and get_slot_pos(active->pos.x) < Constant::columns)
  {
    const Object 
      * const below = immovables.get(get_slot_pos(active->pos, 0, 1)).get(),
      * const on_slot = immovables.get(get_slot_pos(active->pos)).get();//const

    if (below == nullptr and (on_slot == nullptr or !on_slot->can_walk_through()))
      add_slot(get_slot_pos(active->pos, 0, 1), active, end->pos, open_slots, open_map);

    else
    {
      if (get_slot_pos(active->pos.y) - 1 >= 0)
      {
        const Object * const above = immovables.get(get_slot_pos(active->pos, 0, -1)).get();
        if (on_slot != nullptr and on_slot->can_walk_through() and (above == nullptr or above->can_walk_through()))
          add_slot(get_slot_pos(active->pos, 0, -1), active, end->pos, open_slots, open_map);
      }

      if (get_slot_pos(active->pos.x) + 1 >= 0)
      {
        const Object * const right = immovables.get(get_slot_pos(active->pos, 0, 1)).get();
        if (right == nullptr or right->can_walk_through())
          add_slot(get_slot_pos(active->pos, 1, 0), active, end->pos, open_slots, open_map);
      }

      if (get_slot_pos(active->pos.x) - 1 >= 0)
      {
        const Object * const left = immovables.get(get_slot_pos(active->pos, -1, 0)).get();
        if (left == nullptr or left->can_walk_through())
          add_slot(get_slot_pos(active->pos, -1, 0), active, end->pos, open_slots, open_map);
      }

      if (below == nullptr or below->can_walk_through())
        add_slot(get_slot_pos(active->pos, 0, 1), active, end->pos, open_slots, open_map);
    }
  }
}

Point Level::find_path(const Point start, const Point finish)
{
  //A* algorithm

  using Utils::get_slot_pos;
  using Slot_vector = std::vector<std::shared_ptr<Slot>>;

  auto comp = [] (const std::shared_ptr<Slot>& l, const std::shared_ptr<Slot> r) -> bool 
    { return l->full_dist > r->full_dist; };

  std::priority_queue <std::shared_ptr<Slot>, Slot_vector, decltype(comp)> open_slots(comp);
  Slot_map<std::shared_ptr<Slot>> open_map(Constant::rows, Constant::columns);

  std::shared_ptr<Slot> active {new Slot {Point {start.x, start.y}, 0, calc_hypo_dist(start, finish), nullptr}};
  open_slots.push(active);
  open_map.set(get_slot_pos(active->pos), active);
  std::shared_ptr<Slot> end {new Slot {finish, -1, 0, nullptr}};

  while (!open_slots.empty())
  {
    active = open_slots.top();
    open_slots.pop();
    append_open_slots(open_slots, open_map, active, end);

    if (get_slot_pos(active->pos, 0, 0, true) == get_slot_pos(finish, 0, 0, true))
    {
      end->parent = active->parent;
      break;
    }
  }

  while(!open_slots.empty())
    open_slots.pop();//why?

  std::stack<Point> path;
  const Slot* slot = end.get();
  if (end->parent == nullptr)
    return Point {-1, -1};
 
  while (slot->parent != nullptr and slot->parent->parent != nullptr)
  {
    if (slot->pos.x < 0 or slot->pos.x > int(Constant::slot_size * Constant::columns) or 
        slot->pos.y < 0 or slot->pos.y > int(Constant::slot_size * Constant::rows))
      throw std::ios_base::failure("Weird point read");

    path.push(slot->pos);
    slot = slot->parent;
  }
    
    return slot->pos;
}

Point Level::get_input(SDL_Event& e)
{
  Point change;

  if (e.type == SDL_KEYDOWN)
  {
    switch(e.key.keysym.scancode)
    {
      case SDL_SCANCODE_UP:
        change.y = -1;
        break;

      case SDL_SCANCODE_DOWN:
        change.y = 1;
        break;

      case SDL_SCANCODE_LEFT:
        change.x = -1;
        break;

      case SDL_SCANCODE_RIGHT:
        change.x =1;
        break;

      default:
        break;
    }
  }

  return change;
}


void Level::display_all(SDL_Renderer* ren)
{
  Utils::chrono_time_point now = std::chrono::steady_clock::now();
  int milliseconds_since = std::chrono::duration_cast<std::chrono::milliseconds>(now - last_frame).count();

  if (milliseconds_since >= 1000 / Constant::max_fps)
  {
    SDL_RenderClear(ren);

    for (unsigned i = 0; i < objects.size(); ++i)
      if (objects[i]->is_visible())
        objects[i]->render(ren);

    SDL_RenderPresent(ren);
    last_frame = std::chrono::steady_clock::now();
  }
}
