#ifndef LIZA_MOVABLE_H
#define LIZA_MOVABLE_H

#include "my_texture.h"
#include "object.h"


class Movable : public Object
{
public:
  ~Movable() override {}
  
  virtual void move(int xx, int yy) = 0;
  Constant::Object_type get_type() const = 0;

protected:
  Movable() {};
  Movable(int xx, int yy) : Object(xx, yy) {};
};

#endif
