#include "SDL2/SDL.h"
#include "coin.h"
#include "borders.h"


Borders Coin::get_borders() const
{
  Borders b;
 
  b.start.x = get_position().x;
  b.start.y = get_position().y;
  b.end.x = b.start.x + get_size();
  b.end.y = b.start.y + get_size() * Constant::coin_proportion;

  return b;
}

void Coin::render(SDL_Renderer* ren)
{  
  SDL_Rect where;
 
  where.x = get_position().x;
  where.y = get_position().y;
  where.w = get_size();
  where.h = get_size() * Constant::coin_proportion;


  texture.render(ren, where);
}
