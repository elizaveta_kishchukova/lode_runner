#include <SDL2/SDL.h>
#include <fstream>
#include <ios>
#include "constants.h" 
#include "level_maker.h"
#include "my_texture.h"


Level_maker::Level_maker()
{
  SDL_Init(SDL_INIT_VIDEO);
  window = SDL_CreateWindow("God Mode", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                            Constant::window_width, Constant::window_height, 0);
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

  bricks.load_from_file(renderer, Constant::bricks_texture_path);
  stairs.load_from_file(renderer, Constant::stairs_texture_path);
  hero.load_from_file(renderer, Constant::hero_texture_path);
  hero.set_partial_rendering(SDL_Rect {32, 0, 32, 32});

  for (unsigned row = 0; row < Constant::rows; ++row)
    slots[row].fill(Constant::Slot_type::Empty);
}

void Level_maker::run(const std::string& level_path)
{
  while (true) 
  {
    SDL_Event e;

    if (SDL_PollEvent(&e))
    {
      if (e.type == SDL_QUIT) 
        break;
      else if (e.type == SDL_KEYDOWN)
        fill_slot(e);
    }

    SDL_RenderClear(renderer);
    render_slots(renderer);
    SDL_RenderPresent(renderer);
  }

  save_level(level_path);
}

void Level_maker::fill_slot(SDL_Event& e)
{
  int x, y;
  SDL_GetMouseState(&x, &y);
        
  switch(e.key.keysym.scancode)
  {
    case SDL_SCANCODE_1:
      slots[y / Constant::slot_size][x / Constant::slot_size] = Constant::Slot_type::Bricks;
      break;

    case SDL_SCANCODE_2:
      slots[y / Constant::slot_size][x / Constant::slot_size] = Constant::Slot_type::Hero;//do not let more than 1. Delete previous one
      break;

    case SDL_SCANCODE_3:
      slots[y / Constant::slot_size][x / Constant::slot_size] = Constant::Slot_type::Stairs;

    default:
      slots[y / Constant::slot_size][x / Constant::slot_size] = Constant::Slot_type::Empty;
      break;
  }
}

void Level_maker::render_slots(SDL_Renderer* ren)
{
  using namespace Constant;

  for (unsigned row = 0; row < Constant::rows; ++row)
    for (unsigned col = 0; col < Constant::columns; ++col)
      switch(slots[row][col])
      {
        case Slot_type::Bricks:       
          bricks.render(renderer, SDL_Rect {int(col) * slot_size, int(row) * slot_size, slot_size, slot_size});
          break;

        case Slot_type::Hero: 
          hero.render(renderer, SDL_Rect {int(col) * slot_size, int(row) * slot_size, slot_size, slot_size});
          break;

        case Slot_type::Stairs:
          stairs.render(renderer, SDL_Rect {int(col) * slot_size, int(row) * slot_size, slot_size, slot_size});
          break;

        case Slot_type::Empty:
        default:
          break;
        //other types as well, so I'll leave it be a switch
      }
}

void Level_maker::save_level(const std::string& path)
{
  std::ofstream file;
  file.open(path);
  
  for (unsigned row = 0; row < Constant::rows; ++row)
  {
    for (unsigned col = 0; col < Constant::columns; ++col)
      file << int(slots[row][col]) << ' ';

    file << "\n";
  }

//mark new level in levels.txt

  file.close();
}

void Level_maker::close()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

Level_maker::~Level_maker()
{
  close();
}
