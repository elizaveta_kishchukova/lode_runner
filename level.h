#ifndef LIZA_LEVEL_H
#define LIZA_LEVEL_H

#include <SDL2/SDL.h>
#include <vector>
#include <memory>
#include <stack>
#include <queue>
#include "my_texture.h" 
#include "object.h"
#include "bricks.h"
#include "movable.h"
#include "character.h"
#include "constants.h"
#include "coin.h"
#include "door.h"
#include "slot.h"
#include "point.h"
#include "borders.h"
#include "utils.h"
#include "slot_map.h"


class Level
{
  std::vector<std::shared_ptr<Object>> objects;
  Slot_map<std::shared_ptr<Object>> immovables;
  std::vector<std::shared_ptr<Movable>> movables;

  std::vector<std::shared_ptr<Coin>> money;
  std::shared_ptr<Character> hero;
  std::shared_ptr<Door> exit;

  Utils::chrono_time_point last_move;
  Utils::chrono_time_point last_frame;

  int coins, collected_coins;

public:
  Level();
  ~Level() {}

  void run(const std::string& level_path, SDL_Renderer* ren);
  bool is_win() { return true; }//change that to a real win/loss later

private:
  void load_level(const std::string& path);
  void display_all(SDL_Renderer* ren);

  Point get_input(SDL_Event& e);
  bool is_falling(const Object* obj);
  bool can_move(const Object* obj, const Point& direction);

  bool can_move_right(const Object* o);
  bool can_move_left(const Object* o);
  bool can_move_up(const Object* o);
  bool can_move_down(const Object* o);

  void move_hero(SDL_Event& e);
  void move_objects(SDL_Event& e);
  void move_chaser(Character* chaser);
  Point calculate_steps(Point pos, Point to_go);

  Point find_path(const Point start, const Point finish);

  bool greater_slot_ptr(const std::shared_ptr<Slot>& l, const std::shared_ptr<Slot>& r) { return l->full_dist > r->full_dist; }
  int calc_hypo_dist(const Point start, const Point finish);

  template <class Comprator> 
  void add_slot(
    const Point pos, std::shared_ptr<Slot> active, const Point& end_pos, std::priority_queue<std::shared_ptr<Slot>,
    std::vector<std::shared_ptr<Slot>>, Comprator>& open_slots, Slot_map<std::shared_ptr<Slot>>& open_map);

  template <class Comprator> 
  void append_open_slots(
    std::priority_queue<std::shared_ptr<Slot>, std::vector<std::shared_ptr<Slot>>, Comprator>& open_slots, 
    Slot_map<std::shared_ptr<Slot>>& open_map, std::shared_ptr<Slot>& active, const std::shared_ptr<Slot>& end);

  void collect_coins();
  bool is_all_money_collected();
  bool has_won();
};

#endif
