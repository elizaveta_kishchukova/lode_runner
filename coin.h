#ifndef LIZA_COIN_H
#define LIZA_COIN_H

#include <SDL2/SDL.h>
#include "constants.h"
#include "object.h"
#include "borders.h"
#include "my_texture.h"
#include "utils.h"


class Coin : public Object
{
  My_texture& texture;
  int value;

public:
  Coin(int xx = 0, int yy = 0, int val = 1) : Object(xx, yy), texture(Utils::coin_texture), value(val) {} 

  int get_size() const override { return Constant::coin_size; }
  int get_value() const { return value; }

  static Object* create(int x = 0, int y = 0) { return new Coin(x, y); };
  Borders get_borders() const override;
  Constant::Object_type get_type() const override { return Constant::Object_type::Coin; }
  void render(SDL_Renderer* ren) override;
};

#endif

