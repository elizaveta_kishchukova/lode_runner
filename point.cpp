#include "point.h"

Point& Point::operator=(const Point& other)
{
  if(this != &other)
  {
    x = other.x;
    y = other.y;
  }

  return *this;
}

bool Point::operator==(const Point& other) const
{
  return (x == other.x and y == other.y);
}
