#ifndef LIZA_BRICKS_H
#define LIZA_BRICKS_H

#include <SDL2/SDL.h>
#include "object.h"
#include "my_texture.h"
#include "constants.h"
#include "utils.h"


class Bricks : public Object
{
  My_texture& texture;

public:
  Bricks() : texture(Utils::bricks_texture) {}
  Bricks(int xx, int yy) : Object(xx, yy), texture(Utils::bricks_texture) {}
  ~Bricks() override {};

  static Object* create(int x = 0, int y = 0) { return new Bricks(x, y); };
  int get_size() const override { return Constant::slot_size; }
  Borders get_borders() const override;
  Constant::Object_type get_type() const override { return Constant::Object_type::Bricks; }
  void render(SDL_Renderer* ren) override;
};

#endif
