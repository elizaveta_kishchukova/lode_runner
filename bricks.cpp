#include <SDL2/SDL.h>
#include "bricks.h"
#include "my_texture.h"
#include "constants.h"
#include "borders.h"


Borders Bricks::get_borders() const
{
  Borders borders;

  borders.start.x = get_position().x;
  borders.start.y = get_position().y;
 
  borders.end.x = borders.start.x + get_size();  
  borders.end.y = borders.start.y + get_size();

  return borders;
}

void Bricks::render(SDL_Renderer* ren)
{
  SDL_Rect where;
  where.x = get_position().x;
  where.y = get_position().y;
  where.h = get_size();
  where.w = get_size();

  texture.render(ren, where);
}


