#include "../borders.h"
#include "gtest/gtest.h"


TEST(BorderTest, test_the_object)
{
  Borders b1(2, 3, 4, 5);
 
  EXPECT_EQ(3, b1.start.y);
}

int main(int argc, char *argv[])
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
