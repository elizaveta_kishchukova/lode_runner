#include "gtest/gtest.h"
#include "../level_new.h"
#include "../bricks.h"
#include "../door.h"
#include <memory>
#include <fstream>


TEST(Level, Creation)
{
  Level test_level;
  EXPECT_EQ(0, test_level.size());
}

TEST(Level, Add)
{
  Level test_level;
  test_level.add(new Bricks(2, 3));
  EXPECT_EQ(1, test_level.size());
}

TEST(Level, Remove)
{
  Level test_level;
  test_level.add(new Bricks(2, 3));
  EXPECT_EQ(1, test_level.size());
  test_level.remove(0);
  EXPECT_EQ(0, test_level.size());
}

TEST(Level, Get)
{
  Level test_level;
  test_level.add(new Bricks(10, 25));
  EXPECT_EQ(10, test_level.get(0)->get_position().x);
}

TEST(Level, Edit)
{
  Level test_level;
  test_level.add(new Bricks(10, 25));
  test_level.get(0)->set_position(3, 4);
  EXPECT_EQ(3, test_level.get(0)->get_position().x);
}

TEST(Level, Save)
{
  Level test_level;

  test_level.add(new Bricks(10, 25));
  test_level.add(new Bricks(5, 7));
  test_level.add(new Door(100, 120));

  std::string test_path = "levels/test_level.txt";
  test_level.save(test_path);

  std::ifstream file(test_path);
  std::string line;
  getline(file, line);
  EXPECT_EQ("0:10,25", line); 
}

TEST(Level, Load)
{
  Level test_level;
  test_level.load("levels/test_level.txt");

  EXPECT_EQ(10, test_level.get(0)->get_position().x);
  EXPECT_EQ(3, int(test_level.get(2)->get_type()));
}


int main(int argc, char *argv[])
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
