#include "../point.h"
#include <gtest/gtest.h>


TEST(PointTest, first_test)
{
  Point p1 {3, 5};
  Point p2;

  EXPECT_FALSE(p1 == p2);
  
  p1 = p2;
  EXPECT_TRUE(p1.x == p2.x and p1.y == p2.y);
  EXPECT_TRUE(p1 == p2);
}

int main(int argc, char *argv[])
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
