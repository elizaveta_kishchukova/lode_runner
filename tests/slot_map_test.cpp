#include "../slot_map.h"
#include <iostream>


void test_creation()
{
  bool success = true;

  try 
  {
    Slot_map<int> test_obj(-1, -5);
    success = false;
  }
  catch(const std::ios_base::failure& e)
  {
    std::cout << "  PASSED: Exception when attempt to create abnormal object\n";
  };

  if (!success)
    throw std::ios_base::failure("Allowed creation of object with abnormal parameters");
}

void test_set(Slot_map<int>& test_obj)
{
  try
  {
    test_obj.set(0, 0, 1);
    test_obj.set(0, 1, 2);
    test_obj.set(1, 0, 3);
    test_obj.set(1, 1, 4);
    test_obj.set(2, 0, 5);
    test_obj.set(2, 1, 6);
    std::cout << "  PASSED: set() test with correct data\n";
  }
  catch(const std::ios_base::failure& e)
  {
    std::ios_base::failure("Failed set() test with correct data");
  };

  bool success = true;
  try
  {
    test_obj.set(3, 5, 9);
    success = false;
  }
  catch(const std::ios_base::failure& e)
  {
    std::cout << "  PASSED: set() test with incorrect data\n";
  };
  
  if (!success)
    throw std::ios_base::failure("Allowed set() with invalid dimentions");
}

void test_get(Slot_map<int>& test_obj)
{
  int a = 0;

  try
  {
    a = test_obj.get(0, 1);
  }
  catch(const std::ios_base::failure& e)
  {
    throw std::ios_base::failure("Failed to read element");
  };

  if (a != 2)
    throw std::ios_base::failure("Failed to read correct data");
  else
    std::cout << "  PASSED: get() test with correct parameters\n";

  bool success = true;
  try
  {
    test_obj.get(6, 7);
    success = false;
  }
  catch(const std::ios_base::failure& e)
  {
    std::cout << "  PASSED: get() test with abnormal data\n";
  }
  
  if (!success)
    throw std::ios_base::failure("Allowed reading with incorrect parameters");

}

void test()
{
  Slot_map<int> test_obj(2, 3);
  std::cout << "  PASSED: Object creation\n";

  test_creation();
  test_set(test_obj);  
  test_get(test_obj);
}

int main()
{
  std::cout << "Slot_map test started\n";
  test();
  std::cout << "PASSED the test\n"; 
}
