#include "gtest/gtest.h"
#include "../bricks.h"
#include "../stairs.h"
#include "../coin.h"
#include "../door.h"
#include "../hero.h"
#include "../my_texture.h"
#include "../constants.h"
#include "../utils.h"
#include <vector>


TEST(Brick, Brick_Variables_Test)
{
  Bricks brick {0, 100};
  EXPECT_EQ(0, brick.get_position().x);
  EXPECT_EQ(100, brick.get_position().y);
}

TEST(Brick, Brick_Border_Test)
{
  Bricks brick {0, 100};
  int size = brick.get_size();
  EXPECT_EQ(0, brick.get_borders().start.x);
  EXPECT_EQ(100, brick.get_borders().start.y);
  EXPECT_EQ(0 + size, brick.get_borders().end.x);
  EXPECT_EQ(100 + size, brick.get_borders().end.y);
}

TEST(Stairs, Stairs_Variables_Test)
{
  Stairs stairs {0, 100};
  EXPECT_EQ(0, stairs.get_position().x);
  EXPECT_EQ(100, stairs.get_position().y);
}

TEST(Stairs, Stairs_Border_Test)
{
  Stairs stairs {0, 100};
  int size = stairs.get_size();
  EXPECT_EQ(0, stairs.get_borders().start.x);
  EXPECT_EQ(100, stairs.get_borders().start.y);
  EXPECT_EQ(0 + size, stairs.get_borders().end.x);
  EXPECT_EQ(100 + size, stairs.get_borders().end.y);
}

TEST(Coin, Coin_Variables_Test)
{
  Coin coin {0, 100};
  EXPECT_EQ(0, coin.get_position().x);
  EXPECT_EQ(100, coin.get_position().y);
  EXPECT_EQ(1, coin.get_value());
}

TEST(Coin, Coin_Border_Test)
{
  Coin coin {0, 100};
  int size = coin.get_size();
  EXPECT_EQ(0, coin.get_borders().start.x);
  EXPECT_EQ(100, coin.get_borders().start.y);
  EXPECT_EQ(0 + size, coin.get_borders().end.x);
  EXPECT_EQ(100 + int(size * Constant::coin_proportion), coin.get_borders().end.y);
}

TEST(Door, Door_Variables_Test)
{
  Door door;
  door.set_position(0, 100);
  EXPECT_EQ(0, door.get_position().x);
  EXPECT_EQ(100, door.get_position().y);
  EXPECT_EQ(false, door.is_open());
  door.set_open(true);
  EXPECT_EQ(true, door.is_open());
}

TEST(Door, Door_Border_Test)
{
  Door door;
  door.set_position(0, 100);
  int size = door.get_size();
  EXPECT_EQ(0, door.get_borders().start.x);
  EXPECT_EQ(100, door.get_borders().start.y);
  EXPECT_EQ(0 + size, door.get_borders().end.x);
  EXPECT_EQ(100 + size, door.get_borders().end.y);
}

TEST(Hero, Hero_Variables_Test)
{
  Hero hero;
  hero.set_position(0, 100);

  EXPECT_EQ(0, hero.get_position().x);
  EXPECT_EQ(100, hero.get_position().y);
}

TEST(Hero, Hero_Border_Test)
{
  Hero hero;
  hero.set_position(0, 100);
  int size = hero.get_size();

  EXPECT_EQ(0, hero.get_borders().start.x);
  EXPECT_EQ(100, hero.get_borders().start.y);
  EXPECT_EQ(0 + size, hero.get_borders().end.x);
  EXPECT_EQ(100 + size, hero.get_borders().end.y);
}

TEST(Hero, Hero_Set_State_Test)
{
  Hero hero;
  hero.set_position(0, 100);

  EXPECT_EQ(Utils::Facing::Front, hero.get_facing_state());
  hero.set_facing_state(Utils::Facing::Back);
  EXPECT_EQ(Utils::Facing::Back, hero.get_facing_state());
}

TEST(Hero, Hero_Calculation_Test)
{
  Hero hero;
  hero.set_position(0, 100);
  int step = Constant::step;

  hero.move(1, 0);
  EXPECT_EQ(0 + step, hero.get_position().x);
  EXPECT_EQ(100, hero.get_position().y);

  hero.move(0, 1); 
  EXPECT_EQ(0 + step, hero.get_position().x);
  EXPECT_EQ(100 + step, hero.get_position().y);

  hero.move(-1, 0);
  EXPECT_EQ(0, hero.get_position().x);
  EXPECT_EQ(100 + step, hero.get_position().y);
}


int main(int argc, char *argv[])
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
