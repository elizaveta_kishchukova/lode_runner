#ifndef LIZA_HERO_H
#define LIZA_HERO_H

#include "character.h"
#include "constants.h"
#include "utils.h"

class Hero : public Character
{
public:
  Hero() : Character(Utils::hero_textures) {}
  Hero(int x, int y) : Character(Utils::hero_textures, x, y) {}
  ~Hero() override {};

  static Object* create(int x = 0, int y = 0) { return new Hero(x, y); }
  Constant::Object_type get_type() const override { return Constant::Object_type::Hero; }
};

#endif
