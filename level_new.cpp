#include "level_new.h"//
#include "object.h"
#include "bricks.h"//
#include "stairs.h"//
#include "hero.h"//
#include "door.h"//
#include "chaser.h"//
#include "coin.h"//
#include "constants.h"
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>//

//Level::Level(const std::string& path)

Object* Level::get(int index)
{
  if (index >= objects.size())
    throw std::ios_base::failure("Level: Index out of range\n");

  return objects[index].get();
}

void Level::add(Object* ptr)
{
  objects.push_back(std::unique_ptr<Object>(ptr));
 
  Constant::Object_type type = ptr->get_type();

  if (sorted_objects.find(type) == sorted_objects.end())
    sorted_objects.insert(std::make_pair(type, std::vector<Object*>()));
    
  sorted_objects[type].push_back(ptr);
}

void Level::remove(int index)
{
  if (index >= objects.size())
    throw std::ios_base::failure("Level: Index out of range\n");
  Object* ptr = get(index);

  std::vector<Object*>::iterator it;
  Constant::Object_type type = ptr->get_type();
  it = find(sorted_objects[type].begin(), sorted_objects[type].end(), ptr);
  sorted_objects[type].erase(it);
  
  objects.erase(objects.begin() + index);
}

void Level::load(const std::string& path)
{
  std::ifstream file(path);
  if (file.fail())
    throw std::ios_base::failure("Level file doesn't exist\n");

  std::string line;
  while (getline(file, line))
  {
    std::istringstream iss(line);
    int t, x, y; char tmp;
    if (!(iss >> t >> tmp >> x >> tmp >> y)) { break; }

    Constant::Object_type type = Constant::Object_type(t);
    add((*Utils::create_object[type])(x, y));
  }
}

void Level::save(const std::string& path) const
{
  std::ofstream file(path);

  for (int i = 0; i < objects.size(); i++)
    file << int(objects[i]->get_type()) << ":" << objects[i]->get_position().x << "," << objects[i]->get_position().y << "\n";
}
