#ifndef LIZA_CHARACTER_H
#define LIZA_CHARACTER_H

#include <SDL2/SDL.h>
#include <memory>
#include <chrono>
#include "constants.h"
#include "movable.h"
#include "my_texture.h"
#include "borders.h"
#include "utils.h"


class Character : public Movable
{
protected:  
  Utils::Facing facing_state;
  std::vector<std::unique_ptr<My_texture>>& textures;

  Character(std::vector<std::unique_ptr<My_texture>>& t) : textures(t)
    { facing_state = Utils::Facing::Front; }

  Character(std::vector<std::unique_ptr<My_texture>>& t, int xx, int yy) : Movable(xx, yy), textures(t) 
    { facing_state = Utils::Facing::Front; }

  ~Character() override {};

public:
  void move(int xx, int yy) override;

  Borders get_borders() const override;
  Utils::Facing get_facing_state() const { return facing_state; }
  void set_facing_state(Utils::Facing st) { facing_state = st; }

  int get_size() const override { return Constant::character_size; }
  Constant::Object_type get_type() const = 0;
  void render(SDL_Renderer* ren) override;

private:
  void change_animation_status(Utils::Facing main_direction, bool moving, std::chrono::steady_clock::time_point& last_texture_change);
};

#endif
