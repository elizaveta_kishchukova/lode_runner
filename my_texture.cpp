#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "my_texture.h"
#include "point.h"
#include <ios>


My_texture::My_texture()
{
  texture = nullptr;
  part = SDL_Rect();
  width = height = 0; 
}

void My_texture::free()
{
  if (!texture)
  {
    SDL_DestroyTexture(texture);
    texture = nullptr;
    width = height = 0;
  }
}

void My_texture::load_from_file(SDL_Renderer* ren, const std::string& path)
{
  free();

  if (!ren)
    throw std::ios_base::failure("Nullptr renderer\n");

  SDL_Surface *bitmap = IMG_Load(path.c_str());
  if (!bitmap)
  {
    SDL_FreeSurface(bitmap);
    throw std::ios_base::failure("Failed to load an image\n");
  }

  texture = SDL_CreateTextureFromSurface(ren, bitmap);
  if (!texture)
  {
    SDL_FreeSurface(bitmap);
    throw std::ios_base::failure("Failed to create texture\n");
  }

  width = bitmap->w;
  height = bitmap->h;

  SDL_FreeSurface(bitmap);
}

void My_texture::load_from_text(SDL_Renderer* ren, const std::string& text, int size, SDL_Color color, const std::string& font_path)
{
  free();
  
  if(!TTF_WasInit() and TTF_Init()==-1) 
    throw std::ios_base::failure("Failed to Initialized TTF\n");

  TTF_Font* font = TTF_OpenFont(font_path.c_str(), size);
  if (!font)
    throw std::ios_base::failure("Failed to load a font\n");

  SDL_Surface *text_surface = TTF_RenderText_Solid(font, text.c_str(), color);
  if (!text_surface)
    throw std::ios_base::failure("Failed to create a text surface");

  texture = SDL_CreateTextureFromSurface(ren, text_surface);
  if (!texture)
    throw std::ios_base::failure("Failed to create texture\n");

  width = text_surface->w;
  height = text_surface->h;

  SDL_FreeSurface(text_surface);
  TTF_CloseFont(font);
}

void My_texture::render(SDL_Renderer* ren, const SDL_Rect& p_where, const SDL_Rect& p_part)
{
  SDL_Rect where;

  if (p_where.w or p_where.h)//check?
    where = p_where;
  else
    where = SDL_Rect {0, 0, width, height};

  if (p_part.w and p_part.h)
    SDL_RenderCopy(ren, texture, &p_part, &where);
  else if (part.w and part.h)
    SDL_RenderCopy(ren, texture, &part, &where);
  else
    SDL_RenderCopy(ren, texture, 0, &where);
}

My_texture::~My_texture()
{
  free();
}
