#ifndef LIZA_SLOT_H
#define LIZA_SLOT_H

#include "point.h"


struct Slot
{
  Point pos;
  int real_dist;
  int hypo_dist;
  int full_dist;
  Slot* parent;

  Slot(Point p, int r, int h, Slot* par) : pos(p), real_dist(r), hypo_dist(h), parent(par) 
    { full_dist = real_dist + hypo_dist; }
  ~Slot() {}

  bool operator<(const Slot& other) { return full_dist < other.full_dist; }
  bool operator==(const Slot& other) { return full_dist == other.full_dist; }
};

#endif
