#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <fstream>
#include "game.h"
#include "level.h"
#include "my_texture.h"
#include "constants.h"
#include "utils.h"
#include <iostream>//


Game::Game()
{
  SDL_Init(SDL_INIT_VIDEO);
  TTF_Init();
  IMG_Init(IMG_INIT_PNG);
  window = SDL_CreateWindow
  (
    "Lode Runner", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
    Constant::window_width, Constant::window_height, 0
  );

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  Utils::load_textures(renderer);

  std::ifstream file(Constant::levels_list_path);

  if (file.fail())
    throw std::ios_base::failure("Level file doesn't exist\n");

  std::string line;

  while(std::getline(file, line))
    levels.push_back(line);

  exit = false;
}

void Game::run()
{
  while (true) 
  {
    SDL_Event e;
    int level_index = menu(e);

    if (SDL_PollEvent(&e) or exit) 
      if (e.type == SDL_QUIT or exit) 
        break;

    Level level;
    //level.run("levels/" + levels[level_index], renderer);
  }
}

int Game::menu(SDL_Event& e)
{
  int highlight = 0;
  int level = -1;

  while(level == -1)
  {
    display_menu(Constant::Highlight::Level, highlight);
    highlight = act_on_input(e, highlight, &level);

    if (exit)
      break;
  }
  //let the user change name from "player" to something else, create a new layer.

  return level;
}

void Game::display_menu(Constant::Highlight highlight, int level_index)
{
  SDL_RenderClear(renderer);

  My_texture text;
  text.load_from_text(renderer, Constant::menu_description, Constant::d_font_size, Utils::light_gray);
  text.render(renderer);

  for (unsigned i = 0; i < levels.size(); ++i)
  {
    SDL_Color color = Utils::light_gray;
    if (highlight == Constant::Highlight::Level and level_index == int(i))
      color = Utils::white;

    text.load_from_text(renderer, "Level " + std::to_string(i), Constant::d_font_size, color);
    text.render(renderer, SDL_Rect {20, int(30 * (i + 1)), text.get_width(), text.get_height()});
  }

  SDL_RenderPresent(renderer);

  //Display username.
  //take fps settings from level display
}

int Game::act_on_input(SDL_Event& e, int highlight, int* const level)
{
  if (SDL_PollEvent(&e)) 
  {
    if (e.type == SDL_QUIT) 
      exit = true;

    if (e.type == SDL_KEYDOWN)
    {
      switch(e.key.keysym.scancode)
      {
        case SDL_SCANCODE_DOWN:
          if (highlight < int(levels.size()) - 1)
            highlight++;
          break;

        case SDL_SCANCODE_UP:
          if (highlight > 0)
            highlight--;
          break;

        case SDL_SCANCODE_RETURN:
          *level = highlight;
          break;

        case SDL_SCANCODE_ESCAPE:
          exit = true;
          break;

        default:
          break;
      }
    }
  }

  return highlight;
}

void Game::close()
{
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

Game::~Game()
{
  close();
}
