#ifndef LIZA_CHASER_H
#define LIZA_CHASER_H

#include "character.h"
#include "constants.h"
#include "utils.h"

class Chaser : public Character
{
public:
  Chaser() : Character(Utils::chaser_textures) {}
  Chaser(int x, int y) : Character(Utils::chaser_textures, x, y) {}
  ~Chaser() override {};

  static Object* create(int x = 0, int y = 0) { return new Chaser(x, y); }
  Constant::Object_type get_type() const override { return Constant::Object_type::Chaser; }
};

#endif
