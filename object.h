#ifndef LIZA_OBJECT_H
#define LIZA_OBJECT_H

#include <SDL2/SDL.h>
#include "my_texture.h"
#include "borders.h"
#include "point.h"


class Object
{
  Point position;
  bool visible;

protected:
  bool walk_through;

  Object(bool walk_thru = false) : walk_through(walk_thru) { visible = true;}
  Object(int xx, int yy, bool walk_thru = false) : walk_through(walk_thru) { position.x = xx; position.y = yy; visible = true; }

public:
  virtual ~Object() {}

  Point get_position() const { return Point(position.x, position.y); }
  void set_position(int xx, int yy) { position.x = xx; position.y = yy; }
  void set_position(Point& p) { position.x = p.x; position.y = p.y; }

  void set_visible(bool v) { visible = v; }
  bool is_visible() { return visible; }

  bool can_walk_through() const { return walk_through; }

  virtual int get_size() const = 0;
  virtual Borders get_borders() const = 0;
  virtual Constant::Object_type get_type() const = 0;
  virtual void render(SDL_Renderer* ren) = 0;
};

#endif
