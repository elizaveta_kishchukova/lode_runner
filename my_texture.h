#ifndef LIZA_MY_TEXTURE_H
#define LIZA_MY_TEXTURE_H

#include <SDL2/SDL.h>
#include <string>
#include "constants.h"

class My_texture
{
private:
  SDL_Texture* texture;
  SDL_Rect part;
  int width;
  int height;

  void free();

public:
  My_texture();
  virtual ~My_texture();

  void load_from_file(SDL_Renderer* ren, const std::string& path);
  void load_from_text(SDL_Renderer* ren, const std::string& text, int size, SDL_Color color, const std::string& font_path = Constant::default_font_path);

  void set_partial_rendering(const SDL_Rect& p_part) { part = p_part; width = p_part.w; height = p_part.h; }
  void render(SDL_Renderer* ren, const SDL_Rect& p_where = SDL_Rect(), const SDL_Rect& p_part = SDL_Rect());

  int get_width() { return width; }
  int get_height() { return height; }  
};

#endif 
