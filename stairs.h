#ifndef LIZA_STAIRS_H
#define LIZA_STAIRS_H

#include <SDL2/SDL.h>
#include "constants.h"
#include "object.h"
#include "borders.h"
#include "my_texture.h"
#include "utils.h"


class Stairs : public Object
{
  My_texture& texture;

public:
  Stairs() : Object(walk_through = true), texture(Utils::stairs_texture) {}
  Stairs(int xx, int yy) : Object(xx, yy, walk_through = true), texture(Utils::stairs_texture) {}
  ~Stairs() {}

  static Object* create(int x = 0, int y = 0) {return new Stairs(x, y); }
  int get_size() const override { return Constant::slot_size; }
  Borders get_borders() const override;
  Constant::Object_type get_type() const override { return Constant::Object_type::Stairs; }
  void render(SDL_Renderer* ren) override;
};

#endif
