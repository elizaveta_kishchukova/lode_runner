#ifndef LIZA_UTILS_H
#define LIZA_UTILS_H

#include <SDL2/SDL.h>
#include <vector>
#include <memory>
#include <chrono>
#include <map> 
#include "my_texture.h"
#include "borders.h"
#include "object.h"


namespace Utils
{
  typedef Object* (*create_function_t)(int, int);
  using chrono_time_point = std::chrono::steady_clock::time_point;

  extern My_texture 
    background,
    bricks_texture,
    stairs_texture,
    coin_texture,
    door_texture_open,
    door_texture_closed;

  extern std::vector<std::unique_ptr<My_texture>>
    hero_textures,
    chaser_textures;

  extern SDL_Color
    light_gray,
    white;

  enum class Facing
  {
    Front_walking1, Front, Front_walking2,
    Left_walking1, Left, Left_walking2,
    Right_walking1, Right, Right_walking2,
    Back_walking1, Back, Back_walking2 
  };

  void load_textures(SDL_Renderer* ren);

  bool are_colliding(const Borders& b1, const Borders& b2);
  bool are_touching(const Borders& b1, const Borders& b2);

  Facing get_walking1(Facing main_direction);
  Facing get_walking2(Facing main_direction);	
  Facing get_main_direction(Facing state);

  int get_slot_pos(int position, int slot_shift = 0, bool indexes = true);
  Point get_slot_pos(const Point& p, int x_slot_shift = 0, int y_slot_shift = 0, bool indexes = false);
  extern std::map<Constant::Object_type, create_function_t> create_object;

};

#endif
