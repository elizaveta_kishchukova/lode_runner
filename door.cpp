#include <SDL2/SDL.h>
#include "door.h"
#include "borders.h"


Borders Door::get_borders() const
{
  Borders b;

  b.start = get_position();
  b.end.x = b.start.x + get_size();
  b.end.y = b.start.y + get_size();

  return b;
}

void Door::render(SDL_Renderer* ren)
{
  SDL_Rect where;
  where.x = get_position().x;
  where.y = get_position().y;
  where.h = get_size();
  where.w = get_size();

  if (open)
    texture_open.render(ren, where);
  else
    texture_closed.render(ren, where);
}
