#ifndef LIZA_DOOR_H
#define LIZA_DOOR_H

#include <SDL2/SDL.h>
#include "object.h"
#include "constants.h"
#include "borders.h"
#include "utils.h"


class Door : public Object
{
  bool open;
  My_texture& texture_open;
  My_texture& texture_closed;

public:
  Door(int xx = 0, int yy = 0, bool op = false) : Object(xx, yy), open(op), texture_open(Utils::door_texture_open), texture_closed(Utils::door_texture_closed) {}
  ~Door() override {};

  void set_open(bool op) { open = op; }
  bool is_open() { return open; }

  static Object* create(int x = 0, int y = 0) { return new Door(x, y); }
  int get_size() const override { return Constant::slot_size; }
  Constant::Object_type get_type() const override { return Constant::Object_type::Door; }
  Borders get_borders() const override;
  void render(SDL_Renderer* ren) override;
};

#endif
