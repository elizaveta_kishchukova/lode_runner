#ifndef LIZA_POINT_H
#define LIZA_POINT_H


struct Point
{
  int x, y;

  Point() : x(0), y(0) {}
  Point(int xx, int yy) : x(xx), y(yy) {}
  ~Point() {}

  Point& operator=(const Point& other);
  bool operator==(const Point& other) const;
};

#endif

