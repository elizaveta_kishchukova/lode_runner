#include <SDL2/SDL.h>
#include <chrono>
#include "character.h"
#include "constants.h"
#include "borders.h"
#include "utils.h"

Borders Character::get_borders() const
{
  Borders borders;

  borders.start.x = get_position().x;
  borders.start.y = get_position().y;
 
  borders.end.x = borders.start.x + get_size();  
  borders.end.y = borders.start.y + get_size();

  return borders;
}

void Character::move(int xx, int yy)
{
  using namespace Utils;
  static Utils::chrono_time_point last_texture_change = std::chrono::steady_clock::now(); //limit on steps??? step length depend on slot size

  Facing main_direction = Utils::get_main_direction(facing_state);
  bool moving = false;

  if (xx or yy)
  {
    if (xx > 0)
      main_direction = Facing::Right;
    else if (xx < 0)
      main_direction = Facing::Left;
    else 
      main_direction = Facing::Back;

    set_position(get_position().x + xx * Constant::step, get_position().y + yy * Constant::step);
    moving = true;
  }

  change_animation_status(main_direction, moving, last_texture_change);
}

void Character::change_animation_status(Utils::Facing main_direction, bool moving, Utils::chrono_time_point& last_texture_change)
{
  using namespace Utils;

  Utils::chrono_time_point now = std::chrono::steady_clock::now();
  int milliseconds_since = std::chrono::duration_cast<std::chrono::milliseconds>(now - last_texture_change).count();

  if (milliseconds_since >= Constant::milliseconds_per_texture)
  {
    if (moving)
    {
      if (facing_state == main_direction or facing_state == get_walking2(main_direction))
        facing_state = get_walking1(main_direction);
      else if (facing_state == get_walking1(main_direction))
        facing_state = get_walking2(main_direction);
      else 
        facing_state = get_walking1(main_direction);
    }

    else
      facing_state = main_direction;

    last_texture_change = std::chrono::steady_clock::now();
  }
}

void Character::render(SDL_Renderer* ren)
{
  SDL_Rect where;
  where.x = get_position().x;
  where.y = get_position().y;
  where.h = get_size();
  where.w = get_size();

  textures[int(facing_state)]->render(ren, where);
}
