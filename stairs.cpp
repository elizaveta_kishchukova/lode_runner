#include <SDL2/SDL.h>
#include "stairs.h"
#include "borders.h"


Borders Stairs::get_borders() const
{
  Borders b;

  b.start.x = get_position().x;
  b.start.y = get_position().y;
  b.end.x = b.start.x + get_size();
  b.end.y = b.start.y + get_size();

  return b; 
}

void Stairs::render(SDL_Renderer* ren)
{
  SDL_Rect where;
  where.x = get_position().x;
  where.y = get_position().y;
  where.w = get_size();
  where.h = get_size();

  texture.render(ren, where);
}
