#include <SDL2/SDL.h>
#include <memory>
#include "utils.h"
#include "constants.h"
#include "borders.h"
#include "bricks.h"
#include "stairs.h"
#include "chaser.h"
#include "hero.h"
#include "coin.h"
#include "door.h"


My_texture 
  Utils::background,
  Utils::bricks_texture,
  Utils::stairs_texture,
  Utils::coin_texture,
  Utils::door_texture_open,
  Utils::door_texture_closed;

std::vector<std::unique_ptr<My_texture>> 
  Utils::hero_textures,
  Utils::chaser_textures;

SDL_Color
  Utils::light_gray {150, 150, 150},
  Utils::white {255, 255, 255};

Utils::create_function_t test_p = Bricks::create;

std::map<Constant::Object_type, Utils::create_function_t> Utils::create_object 
{
  {Constant::Object_type::Bricks, Bricks::create},
  {Constant::Object_type::Stairs, Stairs::create},
  {Constant::Object_type::Chaser, Chaser::create},
  {Constant::Object_type::Hero, Hero::create},
  {Constant::Object_type::Door, Door::create},
  {Constant::Object_type::Coin, Coin::create}
};

void Utils::load_textures(SDL_Renderer* ren)
{
  using namespace Constant;

  background.load_from_file(ren, background_texture_path);
  bricks_texture.load_from_file(ren, bricks_texture_path);
  stairs_texture.load_from_file(ren, stairs_texture_path);
  coin_texture.load_from_file(ren, coin_texture_path);
  door_texture_open.load_from_file(ren, door_texture_open_path);
  door_texture_closed.load_from_file(ren, door_texture_closed_path);

  int 
    i = 0,
    start_x = 0,
    start_y = 0;

  for(unsigned row = 0; row < num_tex_height; ++row)
    for (unsigned col = 0; col < num_tex_width; ++col)
    {
      start_x = col * char_tex_size;
      start_y = row * char_tex_size;

      hero_textures.push_back(std::unique_ptr<My_texture> (new My_texture()));
      hero_textures[i]->load_from_file(ren, hero_texture_path);
      hero_textures[i]->set_partial_rendering(SDL_Rect {start_x, start_y, char_tex_size, char_tex_size});

      chaser_textures.push_back(std::unique_ptr<My_texture> (new My_texture()));
      chaser_textures[i]->load_from_file(ren, chaser_texture_path);
      chaser_textures[i]->set_partial_rendering(SDL_Rect {start_x, start_y, char_tex_size, char_tex_size});
      
      ++i;
    }  
}

bool Utils::are_touching(const Borders& b1, const Borders& b2)
{
  if 
  (
    (b1.start.y == b2.end.y + 1 and b1.start.x <= b2.end.x and b1.end.x >= b2.start.x) or 
    (b2.start.y == b1.end.y + 1 and b1.start.x <= b2.end.x and b1.end.x >= b2.start.x) or
    (b1.start.x == b2.end.x + 1 and b1.start.y <= b2.end.y and b1.end.y >= b2.start.y) or 
    (b2.start.x == b1.end.x + 1 and b1.start.y <= b2.end.y and b1.end.y >= b2.start.y)
  )
    return true;

  return false;
}

bool Utils::are_colliding(const Borders& b1, const Borders& b2)
{
  bool 
    x_colliding = false,
    y_colliding = false;

  if (b1.start.x <= b2.end.x and b1.end.x >= b2.start.x)
    x_colliding = true;

  if (b1.start.y <= b2.end.y and b1.end.y >= b2.start.y)
    y_colliding = true;


  return (x_colliding and y_colliding);
}

Utils::Facing Utils::get_walking1(Facing main_direction)
{
  return Facing(int(main_direction) - 1);
}

Utils::Facing Utils::get_walking2(Facing main_direction)
{
  return Facing(int(main_direction) + 1);
}

Utils::Facing Utils::get_main_direction(Facing state)
{
  if (int(state) % 3 == 0)
    return Facing(int(state) + 1);
  else if (int(state) % 3 == 1)
    return state;
  else
    return Facing(int(state) - 1);
}

int Utils::get_slot_pos(int position, int slot_shift, bool indexes)
{
  if (indexes)
    return position / Constant::slot_size + slot_shift;
  else
    return position + slot_shift * Constant::slot_size;
}

Point Utils::get_slot_pos(const Point& p, int x_slot_shift, int y_slot_shift, bool indexes)
{
  if (indexes)
    return Point {get_slot_pos(p.x, x_slot_shift, true), get_slot_pos(p.y, y_slot_shift, true)};
  else
    return Point {get_slot_pos(p.x, x_slot_shift, false), get_slot_pos(p.y, y_slot_shift, false)};
}
