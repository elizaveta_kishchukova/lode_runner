#ifndef LIZA_CONSTANTS_H
#define LIZA_CONSTANTS_H

#include <SDL2/SDL.h>
#include <string>


namespace Constant
{
  const unsigned 
    window_width = 1000,//get this value from settings
    window_height = window_width * 2 / 3,

    columns = 30,
    rows = 20,

    num_char_tex = 10,
    num_tex_height = 4, 
    num_tex_width = 3,

    d_font_size = 20;

  const int 
    char_tex_size = 32,
    slot_size = window_width / columns,
    character_size = 0.8 * slot_size,
    coin_size = 0.5 * slot_size,

    milliseconds_per_texture = 250,
    milliseconds_per_move = 10,
    max_fps = 45,
    step = 2;

  const double coin_proportion = 13.0 / 28.0;

  const std::string 
    background_texture_path = "media/background.bmp",
    bricks_texture_path = "media/bricks.png",
    stairs_texture_path = "media/stairs.png",
    coin_texture_path = "media/coin.png",
    hero_texture_path = "media/hero.png",
    chaser_texture_path = "media/chaser.png",
    door_texture_open_path = "media/door_open.png",
    door_texture_closed_path = "media/door_closed.png",

    save_level_path = "levels/1.dat",//temp
    levels_list_path = "levels/levels.txt",
    default_font_path = "media/Ubuntu-R.ttf",
    menu_description = "To move between layers, use \"up\" and \"down\" arrows. To play - press Enter";

  enum class Slot_type
    {Empty, Bricks, Hero, Stairs, Coin, Door};

  enum class Object_type
    {Bricks, Stairs, Coin, Door, Empty, Hero, Chaser};  //type and idea being not the same thing

  enum class Highlight
    {Nothing, Level, Exit, New_Level};
}

#endif

