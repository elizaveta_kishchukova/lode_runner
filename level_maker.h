#ifndef LIZA_LEVEL_MAKER_H
#define LIZA_LEVEL_MAKER_H

#include <SDL2/SDL.h>
#include <array>
#include "constants.h" 
#include "my_texture.h"


class Level_maker
{
private:
  SDL_Window* window;
  SDL_Renderer* renderer;

  My_texture bricks;
  My_texture stairs;
  My_texture hero;

  std::array<std::array<Constant::Slot_type, Constant::columns>, Constant::rows> slots;

public:
  Level_maker();
  ~Level_maker();

  void run(const std::string& level_path);
  void close();

private:  
  void fill_slot(SDL_Event& e);
  void render_slots(SDL_Renderer* ren);
  void save_level(const std::string& path);
};

#endif
