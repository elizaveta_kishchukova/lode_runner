#ifndef LIZA_BORDERS_H
#define LIZA_BORDERS_H

#include "point.h"


struct Borders
{
  Point start;
  Point end;

  Borders() : start(Point{0, 0}), end(Point{0, 0}) {}
  Borders(const Point& s, const Point& e) : start(s), end(e) {}
  Borders(int x1, int y1, int x2, int y2) : start(Point{x1, y1}), end(Point{x2, y2}) {}

  ~Borders() {}
};

#endif
