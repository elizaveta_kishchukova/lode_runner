#ifndef LIZA_GAME_H
#define LIZA_GAME_H

#include <SDL2/SDL.h>
#include <vector>
#include "constants.h"


class Game
{
  SDL_Window *window;
  SDL_Renderer *renderer;

  std::vector<std::string> levels;
  bool exit;

public:
  Game();
  ~Game();

  void run();
  void close();

private:
  int menu(SDL_Event& e);
  void display_menu(Constant::Highlight highlight = Constant::Highlight::Nothing, int level_index = -1);
  int act_on_input(SDL_Event& e, int highlight, int* const level);
};

#endif
