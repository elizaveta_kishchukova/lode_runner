#ifndef LIZA_SLOT_MAP_H
#define LIZA_SLOT_MAP_H

#include <vector>
#include <ios>
#include "point.h"
#include "utils.h"
#include <iostream>//

 
template <class T>
class Slot_map
{
  std::vector<std::vector<T>> map;
  int size_rows, size_cols;

public:
  Slot_map(int rows, int columns);
  ~Slot_map(){}

  T get(int row_index, int col_index);
  T get(Point pos);
  void set(int row_index, int col_index, T new_value);
  void set(Point pos, T new_value);

private:
  void check_range(int row, int col);
};


template <class T>
Slot_map<T>::Slot_map(int rows, int columns)
{ 
  if (rows < 0 or columns < 0)
    throw std::ios_base::failure("Row and column measurement can only be greater or equal to zero\n");

  size_rows = rows;
  size_cols = columns;

  for (int r = 0; r < rows; ++r)
  {
    map.push_back(std::vector<T>());
 
    for (int c = 0; c < columns; ++c)
      map[r].push_back(T());
  }
}

template <class T>
T Slot_map<T>::get(int row_index, int col_index)
{
  check_range(row_index, col_index);

  return map[row_index][col_index];
}

template <class T>
T Slot_map<T>::get(Point pos)
{
  int row = Utils::get_slot_pos(pos.y);
  int col = Utils::get_slot_pos(pos.x);

  return get(row, col);
}

template <class T>
void Slot_map<T>::set(int row_index, int col_index, T new_value)
{
  check_range(row_index, col_index);

  map[row_index][col_index] = new_value;
}

template <class T>
void Slot_map<T>::set(Point pos, T new_value)
{
  int row = Utils::get_slot_pos(pos.y);
  int col = Utils::get_slot_pos(pos.x);

  set(row, col, new_value);
}

template <class T>
void Slot_map<T>::check_range(int row, int col)
{
  if (row < 0 or row >= size_rows or col < 0 or col >= size_cols)
    throw std::ios_base::failure("Index out of range. ");
}

#endif
