#ifndef LIZA_NEW_LEVEL_H
#define LIZA_NEW_LEVEL_H

#include <memory>
#include <vector>
#include <string>
#include "object.h"
#include "movable.h"
#include "coin.h"
#include "character.h"
#include "door.h"
#include <map>


class Level
{
  std::vector<std::unique_ptr<Object>> objects;
  std::map<Constant::Object_type, std::vector<Object*>> sorted_objects;

public:
  Level() {}
  ~Level() {}

  void add(Object* ptr);

  Object* get(int index);
  void remove(int index);
  int size() const { return objects.size(); }

  void load(const std::string& path);
  void save(const std::string& path) const;
};

#endif
